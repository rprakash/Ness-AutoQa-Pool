package ness.qa.pool.automation.suite.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ness.autoqa.pool.sdk.MyAppTestCase;
import ness.autoqa.pool.sdk.TestDataCollection;
import ness.autoqa.pool.sdk.TestDataReader;
import ness.autoqa.pool.sdk.TestDataReaderFactory;
import ness.autoqa.pool.sdk.homepage.HomePage;
import ness.autoqa.pool.sdk.homepage.VerifyHomePage;

/**
 * Contains test-cases to verify that download is working correctly and its verifications 
 */
public class test extends MyAppTestCase {
    private String HomePageUrl = "http://www.seleniumhq.org/";
    private String correctUrl = "http://www.seleniumhq.org/download/";

    private HomePage homePage;
    private VerifyHomePage verifyHomePage;

    @BeforeClass
    public void instantiateObject() {
        homePage = new HomePage(driver);
        verifyHomePage = new VerifyHomePage(driver);
    }

    /**
     * Navigates to selenium home page and clicks on download link
     */
    @Test
    public void login() {
        driver.get(HomePageUrl);
        homePage.navigateToDownloadPage();
        verifyHomePage.hasNavigatedToCorrectPage(correctUrl);
    }
}

