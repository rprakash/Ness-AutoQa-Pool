# Selenium Page Object Model using ANT

This framework is composed of two things:

* Selenium tests
* A custom SDK to support these tests

The latter (SDK) is the actual framework.

## SDK

The SDK provides convenience methods that help write tests efficiently. All the methods that used to be known as _common methods_ now live in the SDK.

#### Page Objects

We use the [Page Objects pattern](https://code.google.com/p/selenium/wiki/PageObjects) to model our views.

All the views (the _pages_) in the given web application are represented in our SDK as Page Objects.


##### How to query DOM elements

preferred methods are:-

1. Id or name
2. Tag or class name
3. CSS selectors ([learn how to write them efficiently](https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Writing_efficient_CSS)!)
4. Link or partial link text
5. Xpaths

#### Waits
We have a central class that will help you wait for things to happen, called `Wait`.

This class implements wait mechanisms in the most efficient way possible (using fluent / explicit waits). It does the heavy lifting for you.

Let's say you want to click on an element and wait for another element to appear. That's a fairly complex thing to do, but now it's trivial because you can use `Wait.untilClickDisplaysElement`

The __timeouts__ are specified in a configuration file and can be changed with ease.

##### Disabling implicit waits

When you try to find an element, for example using `driver.findElement()`, Selenium uses an _implicit wait_. It will keep trying to find this element until it times out.


#### Verification

Each module has a class that performs different verifications.

#### Re-running failed test-cases

failed test-cases can be re-run by using -DmaxRetryCount='count'

#### Configuration and test data readers

All our file readers implement a clever caching mechanism that greatly improves the overall performance, please use these classes to read configuration or test data files instead of doing it directly.

You can use the `Configuration` class to read any of the configuration options. It implements a singleton pattern so you can get the instance and then obtain the desired configuration value:

```java
String url = Configuration.getInstance().get("url");
```

We have a YAML reader for test data, to allow us to work with collections. Please use the `TestDataReaderFactory` class to obtain a reader.


#### Generic TestCase class

All test case classes must inherit from `sdk.MyAppTestCase`

This class doesn't have any inheritable methods, it is only used to implement a __common test lifecycle__.

#### Other utilities

In the `ness.autoqa.pool.sdk` there are other utility classes, such as:

* `BrowserUtils`
* `Environment`
* `WebDriverFactory`

#### Libraries

* Selenium: 2.44
* TestNG: 6.8
* Snake YAML: 1.13
* Saxon (for reports): 8.7
* Saxon Liaison
* JXL

#### Web drivers

* Chrome: 2.39
* Internet Explorer: 2.39

## Selenium tests

### TestNG suite files

All XML files for TestNG suites are in the `testng-xml-suites` folder. There is a file per package, and this file defines one suite.

That suite willrun from ant using:

```
ant -Dsuite='suiteName'


### Test Data

YAML is the language for test data. It gives us great flexibility when writing the test data files and allows us to work with collections. It's like reading objects from a text file, let's look at an example.


#### Java

To start, here are links for downloading [Java JRE 1.7](http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html) and [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html).
Open a command line tool to test for Java’s proper installation.

#### Mac OSX
Mac OSX users can simply open Terminal.'

#### Windows

Windows users can open the command line by entering cmd from the start screen.

Once your command line is available, issue the following command.

```
java -version
```
If correctly installed, it should produce the following version information:

```
>java -version
java version "1.7.0_40"
Java(TM) SE Runtime Environment (build 1.7.0_40-b43)
Java HotSpot(TM) 64-Bit Server VM (build 24.0-b56, mixed mode)
```

For a bit more information, the Java Runtime Environment is an implementation of the Java Virtual Machine that executes Java programs. Without the JRE, you can compile an application but can’t run it.

The Java Development Kit (JDK) is a software bundle that you can use to develop Java-based software. Since the JRE contains the JVM which executes the bytecode generated from the javac compiler, you need to add your JRE path as the JAVA_HOME variable from the environment variables.

Note: The JDK contains the JRE, so if you have set your PATH properly, you shouldn’t need a separate entry for the JRE.

#### Adding Java to your PATH

#### Windows

Add the following bits of information to your System Variables to add Java to your PATH:

```
variable name: JAVA_HOME
variable value: c:\jdk1.7.0_40
variable name: PATH
variable value: %PATH%;%JAVA_HOME%\bin\
```

#### Mac OSX / Linux

Unless you have intentionally changed your Java path, Java is most likely installed here:

```
/Library/Java/JavaVirtualMachines/<java version>/.
```
Add the following lines to your ~/.bash_profile to add Java to your PATH:

```
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_40.jdk/Contents/Home
export PATH=$JAVA_HOME/bin:$PATH
For more information see: http://java.com/en/download/help/path.xml
```
#### ANT

Install Apache Ant, download it [here](https://ant.apache.org/bindownload.cgi).

Adding Ant to your PATH

#### Windows

Unless otherwise specified, Windows users can assume Ant is installed at c:\ant. Add the following bits of information to your System Variables to add Ant to your PATH:

variable name: ANT_HOME
variable value: c:\ant
variable name: PATH
variable value: %PATH%;%ANT_HOME%\bin\
```

#### Mac OSX / Linux users

Unless otherwise specified, Mac and Linux users can assume Ant is installed at /usr/bin/ant. Add the following lines to your ~/.bash_profile to add Ant to your PATH:

export ANT_HOME=/usr/bin/ant
export PATH=${PATH}:${ANT_HOME}/bin

### Running the suite

You can run it using ant or directly in Eclipse. To run all the suites navigate to the root directory of project:

ant

To run a specific suite (where _nameOfSuite_ is the name of the xml file from `testng-xml-suites` excluding the extension):

ant -Dsuite=nameOfSuite


The report is generated in the _report_ folder, but only when you run the suites using ant.

#### Configuring the execution

You will find all the available configuration options in the `default.properties` file, with their default values. You __cannot__ make changes to this file, to override any of these settings create a `build.properties` file and specify them there.

You can also specify options from the command line, like this:

```
ant -Dgrid.enabled=true
```

To run all suites without grid you can issue command like this:

```
ant -Durl=http://your-installation-name.com -Dbrowser.name=Chrome
```
/* By default it chooses firefox to run the suites but you can specify other browsers as -Dbrowser.name=Chrome.

The command line options take precedence over `build.properties`, and that takes precedence over `default.properties`

Eclipse won't read `build.properties`, to parametrize an execution from Eclipse you need to pass the configurations as environment variables. To do so, choose _Run Configurations..._ (or _Debug Configurations..._) and add new variables to the _Environment_ tab.

__WARNING:__ the `url` property is not set in `default.properties`. You must specify where the tests will be run. Try to use a local development server to not affect other executions.

If you installed the TestNG plugin in Eclipse you can right click on a method, class or xml file and choose to run it as a TestNG test (or suite). The execution will fail because it will be missing the `url` parameter. Open the newly created configuration as described above and add the url as an environment variable.

Notice that with the TestNG plugin it is very easy to create a custom _run configuration_ that can target a class, a method or multiple suite files. All your run configurations will be available as debug configurations and vice-versa.

## Running in a VM
If you want to run the Selenium tests locally, you should do so in a virtual machine. Otherwise you will be unable to do anything ale on your computer while the tests are executed.
