package ness.autoqa.pool.sdk;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

/**
 * Common test life cycle for all classes that will test Fronter (main application)
 * 
 * No methods will be inherited using this class, only common life cycle requirements.
 * Implement common methods in the appropriate SDK class.
 * Keep this class to a minimum.
 */
public class MyAppTestCase {
    protected WebDriver driver;


    @BeforeClass
    public void instantiateDriver() {
        driver = WebDriverFactory.getInstance().getDriver();
    }

    @BeforeClass
    public void logCurrentClassName() {
        System.out.println(this.getClass().getSimpleName());
    }

    @AfterMethod
    public void logTestStatus(final ITestResult testResult) {
        if (!testResult.isSuccess()) {
            System.err.println(testResult.getMethod().getMethodName() + ": Failed");
        }
    }

    @AfterClass(alwaysRun = true)
    public void closeBrowser() {
        driver.quit();
    }
}
