package ness.autoqa.pool.sdk.homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {  
    @FindBy(xpath = "//a[@title='Get Selenium']")
    WebElement downloadLink;
    
    WebDriver driver;
    public HomePage(WebDriver driver) {
        this.driver=driver;
    }

    /**
     * Open page to download selenium
     */
    public void downloadSelenium() {
        downloadLink.click();
    }

    /**
     * Verifies the current url
     * 
     * @param url
     * @return
     */
    public boolean hasNavigatedToCorrectPage(String url) {
        try {
            return driver.getCurrentUrl().equalsIgnoreCase(url);
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * navigates to download page
     * 
     * @param url
     */
    public void navigateToDownloadPage() {
        downloadLink.click();
    }

}
