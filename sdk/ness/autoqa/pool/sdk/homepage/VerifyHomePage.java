package ness.autoqa.pool.sdk.homepage;

import org.openqa.selenium.WebDriver;

public class VerifyHomePage {
    private WebDriver driver;
    private HomePage homePage;

    public VerifyHomePage(WebDriver driver) {
        this.driver = driver;
        homePage = new HomePage(driver);
    }

    /**
     * Verifies the current url
     * 
     * @param url
     * @return
     */
    public void hasNavigatedToCorrectPage(String url) {
        homePage.hasNavigatedToCorrectPage(url);
    }

}
