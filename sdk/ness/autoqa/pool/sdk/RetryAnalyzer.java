package ness.autoqa.pool.sdk;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

/**
 * This class tells TestNG if a failed test should be run again
 * Important: Ideally before each retry it should reset the building database, otherwise test cases will get executed
 * with junk data. But retry code only retries test case method so implementation of database reset is not possible.
 */
public class RetryAnalyzer implements IRetryAnalyzer {
    private int retryCount = 0;
    private Configuration config = Configuration.getInstance();
    private int maxRetryCount = Integer.parseInt(config.get("maxRetryCount"));

    @Override
    public boolean retry(ITestResult result) {
        if (canRetry()) {
            retryCount++;
            Reporter.log("Retrying (" + retryCount +") " + result.getName());
            return true;
        }
        return false;
    }

    /**
     * Evaluates if the current test could potentially be rerun
     *
     * @return Whether if the test can be executed at least one more time
     */
    public boolean canRetry() {
        return retryCount < maxRetryCount;
    }
}
