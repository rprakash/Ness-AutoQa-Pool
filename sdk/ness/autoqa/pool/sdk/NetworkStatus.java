package ness.autoqa.pool.sdk;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Checks if host server is reachable or not
 */
public class NetworkStatus {
    private int timeout = 2000;

    public void pingServer() {
        String ipAddress = Configuration.getInstance().get("ipAddress");
        InetAddress inet = null;

        if (ipAddress != null) {
            try {
                inet = InetAddress.getByName(ipAddress);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            System.out.println("Sending Ping Request to " + ipAddress);
            try {
                System.out.println(inet.isReachable(timeout) ? "Host is reachable" : "Host is NOT reachable");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
