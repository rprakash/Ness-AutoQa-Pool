package ness.autoqa.pool.sdk;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverFactory {
    private static WebDriverFactory instance = null;
    private Configuration config;
    private String gridHubUrl;
    private String hubUrlPath = "/wd/hub";
    private int implicitWaitSeconds;

    public static WebDriverFactory getInstance() {
        if (instance == null) {
            instance = new WebDriverFactory();
        }
        return instance;
    }

    protected WebDriverFactory() {
        config = Configuration.getInstance();
        implicitWaitSeconds = Integer.parseInt(config.get("selenium.implicitWaitSeconds"));
        String isGridEnabled = config.get("grid.enabled");
        String targetBrowser = config.get("browser.name");

        if (targetBrowser.equalsIgnoreCase("mobileapp")) {
            gridHubUrl = config.get("appium.server.url");
        } else if (isGridEnabled != null && isGridEnabled.equals("true")) {
            gridHubUrl = config.get("grid.hub");
        }
    }

    /**
     * Creates a driver to be used with the grid or mobile devices
     * 
     * @param hubUrl Url where the Selenium grid/appium hub is located
     * @return Driver instance
     */
    private WebDriver createRemoteWebDriver(String hubUrl) {
        RemoteWebDriver driver = null;
        DesiredCapabilities capabilities;
        String targetBrowser = config.get("browser.name");
        switch (targetBrowser.toLowerCase()) {
            case "mobileapp":
                capabilities = generateMobileCapabilities();
                break;
            case "firefox":
            default:
                // Firefox is used by default
                capabilities = DesiredCapabilities.firefox();
        }
        System.out.println("Starting remote driver on " + gridHubUrl + " for execution on " + targetBrowser);
        System.out.println("Starting remote driver on " + gridHubUrl);
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), capabilities);
            driver.setFileDetector(new LocalFileDetector());
        } catch (Exception e) {
            System.err.println("Error starting remote driver: " + e.getMessage());
            System.exit(-1);
        }
        return driver;
    }

    /**
     * Creates a driver to be used in local executions, based on the configuration settings
     * 
     * @return Driver instance
     */
    private WebDriver createLocalWebDriver() {
        WebDriver driver;
        Platform currentPlatform = Platform.getCurrent();
        Configuration config = Configuration.getInstance();
        String targetBrowser = config.get("browser.name");
        implicitWaitSeconds = Integer.parseInt(config.get("selenium.implicitWaitSeconds"));

        System.out.println("Running locally using " + targetBrowser);
        switch (targetBrowser.toLowerCase()) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", BrowserUtils.getChromeDriverPath());
                driver = new ChromeDriver();
                break;
            case "safari":
                if (!currentPlatform.is(Platform.MAC)) {
                    System.err.println("Safari is only supported on Mac OS");
                    System.exit(-1);
                }
                driver = new SafariDriver();
                break;
            case "ie":
            case "internet_explorer":
                if (!currentPlatform.is(Platform.WINDOWS)) {
                    System.err.println("Internet Explorer is only supported on Windows");
                    System.exit(-1);
                }
                DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
                caps.setCapability("ignoreZoomSetting", true);
                System.setProperty("webdriver.ie.driver", BrowserUtils.getIEDriverPath());
                driver = new InternetExplorerDriver(caps);
                break;
            case "firefox":
            default:
                FirefoxProfile profile = new FirefoxProfile();
                driver = new FirefoxDriver(profile);
        }

        return driver;
    }

    /**
     * Get the current driver
     * 
     * @return Driver used by all the classes
     */
    public WebDriver getDriver() {
        String targetBrowser = config.get("browser.name");
        WebDriver driver;
        if (gridHubUrl != null && !gridHubUrl.isEmpty()) {
            driver = createRemoteWebDriver(gridHubUrl + hubUrlPath);
        } else {
            driver = createLocalWebDriver();
        }
        driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
        if (!targetBrowser.equalsIgnoreCase("mobileapp")) {
            driver.manage().window().maximize();
        }
        return driver;
    }

    /**
     * Disable the timeout applied when looking for DOM elements.
     * This is very useful when checking if a node has been destroyed.
     * 
     * IMPORTANT: remember to call enableImplicitWaits() after the assertion has been performed
     * 
     * @param driver Driver for which to disable the implicit waits
     * @see #enableImplicitWaits
     */
    public void disableImplicitWaits(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    /**
     * Restores the original timeout used in DOM queries
     * 
     * @param driver Driver for which to enable the implicit waits
     */
    public void enableImplicitWaits(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
    }

    /**
     * This will set desired capabilities for mobile
     * 
     * @return capabilities
     */
    public DesiredCapabilities generateMobileCapabilities() {
        DesiredCapabilities mobileCapabilities = new DesiredCapabilities();
        mobileCapabilities.setCapability("platformName", config.get("mobile.platform.name"));
        mobileCapabilities.setCapability("platformVersion", config.get("mobile.platform.version"));
        mobileCapabilities.setCapability("deviceName", config.get("mobile.device.name"));
        mobileCapabilities.setCapability("device-orientation", config.get("mobile.device.orientation"));

        String mobileAppPath = config.get("mobile.app.path");
        String mobileBrowserName = config.get("mobile.browser.name");
        String mobileUdid = config.get("mobile.udid");
        String mobileBundleId = config.get("mobile.bundleId");
        String mobileDeviceId = config.get("mobile.device.id");
        String mobileAppPacakge = config.get("mobile.app.package");
        String mobileAppActivity = config.get("mobile.app.activity");

        // For running tests on an mobile app
        if (mobileAppPath != null) {
            mobileCapabilities.setCapability("app", mobileAppPath);
        }

        // For running tests on mobile browser
        if (mobileBrowserName != null) {
            mobileCapabilities.setCapability("browserName", mobileBrowserName);
        }

        // For running tests on native/hybrid app on real iOS device
        if (mobileUdid != null && mobileBundleId != null) {
            mobileCapabilities.setCapability("udid", mobileUdid);
            mobileCapabilities.setCapability("bundleId", mobileBundleId);
        }

        // Few more capabilities required for running tests on Android devices
        if (mobileDeviceId != null && mobileAppPacakge != null && mobileAppActivity != null) {
            mobileCapabilities.setCapability("device ID", mobileDeviceId);
            mobileCapabilities.setCapability("appPackage", mobileAppPacakge);
            mobileCapabilities.setCapability("appActivity", mobileAppActivity);
        }

        return mobileCapabilities;
    }
}
