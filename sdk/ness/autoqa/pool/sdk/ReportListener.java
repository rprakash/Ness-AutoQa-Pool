package ness.autoqa.pool.sdk;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Contains listeners to be used during the test execution. No need to call this class or its instance anywhere.
 */
public class ReportListener extends TestListenerAdapter {
    @Override
    public void onTestFailure(ITestResult result) {
        NetworkStatus networkStatus = new NetworkStatus();
        networkStatus.pingServer();
        RetryAnalyzer analyzer = (RetryAnalyzer) result.getMethod().getRetryAnalyzer();

        if (!analyzer.canRetry()) {
            // Obtain the screenshot
            Object currentClass = result.getInstance();
            WebDriver webDriver = ((MyAppTestCase) currentClass).driver;
            File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);

            // Create the destination folder
            String destDir = Configuration.getInstance().get("tests.report");
            destDir += "/screenshots";

            // Create the folder that contains the screenshots (if it doesn't exist)
            new File(destDir).mkdirs();

            String destFile = destDir + "/" + result.getName() + ".png";
            try {
                FileUtils.copyFile(scrFile, new File(destFile));
            } catch (IOException e) {
                System.err.println("Could not copy the screenshot file to " + destFile);
                e.printStackTrace();
            }

            super.onTestFailure(result);
        }
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        super.onTestSuccess(result);

        // If this test failed previously remove it from the list of failures
        if (result.getMethod().getCurrentInvocationCount() > 1) {
            ITestContext context = result.getTestContext();
            Iterator<ITestNGMethod> failedMethods = context.getFailedTests().getAllMethods().iterator();
            while (failedMethods.hasNext()) {
                String methodName = failedMethods.next().getMethodName();
                if (methodName.equals(result.getMethod().getMethodName())) {
                    failedMethods.remove();
                }
            }

            // We control when onTestFailure is run on the parent class and therefore this
            // shouldn't be necessary, but we want to make sure the list is clean
            Iterator<ITestResult> failedTests = super.getFailedTests().iterator();
            while (failedTests.hasNext()) {
                ITestResult failedTest = failedTests.next();
                String methodName = failedTest.getMethod().getMethodName();
                if (methodName.equals(result.getMethod().getMethodName())) {
                    failedTests.remove();
                }
            }
        }
    }

    @Override
    public void onFinish(ITestContext context) {
        // Remove duplicated or false failures
        Iterator<ITestResult> failedTests = context.getFailedTests().getAllResults().iterator();
        while (failedTests.hasNext()) {
            ITestResult failedTest = failedTests.next();
            ITestNGMethod method = failedTest.getMethod();
            if (context.getFailedTests().getResults(method).size() > 1) {
                failedTests.remove();
            } else if (context.getPassedTests().getResults(method).size() > 0) {
                failedTests.remove();
            }
        }

        // In case of failed or skipped tests a file is written to tell ant that the build must fail.
        if ((context.getFailedTests().size() + context.getSkippedTests().size()) > 0) {
            writeFailureFile(context.getOutputDirectory());
        }

        super.onFinish(context);
    }

    /**
     * Write a file to tell ant that the build must fail
     * 
     * @param directory Output directory for this test
     */
    private void writeFailureFile(String directory) {
        // Each context's output directory is a separated sub-folder, the file will be stored in the parent folder
        String[] folders = directory.split("\\" + File.separator);
        String path = "";
        // Don't include the last folder, as we want to store it in its parent
        for (int i = 0; i < folders.length - 1; i++) {
            path += folders[i] + File.separator;
        }
        path += "tests-failed";
        File tempFile = new File(path);
        try {
            tempFile.createNewFile();
        } catch (IOException e) {
            System.err.println("Could not create failure file");
        }
    }
}
